#include<stdio.h>

const int first_arr[] = {1,2,3}, second_arr[]={1,3,2};


long int scalar_product(const int* a, const int* b, size_t size)
{
	long int sum = 0;
	for(size_t i = 0; i < size; i++ ){
		sum += (long int) a[i] * b[i];
	}
	return sum;
}


int main (int argc, char** argv)
{
	size_t size = sizeof(first_arr)/sizeof(int);
	printf("The result = %li\n", scalar_product(first_arr, second_arr, size) );
	return 0;
}
