#include<stdio.h>
#include<time.h>
#include<math.h>

int is_prime(unsigned long number)
{	unsigned long i;
	if(number == 2) return 1;
	if(number <= 1 || number%2 == 0 ) return 0;
	for(i=3; i*i < number; i+=2){
		if(number%i==0) return 0;
	}
	return 1;

}

int fast_is_prime(unsigned long number)
{	unsigned long i;
	if(number == 2) return 1;
	if(number <= 1 || number%2 == 0 ) return 0;
	double node = sqrt(number);
	for(i=3; i < node; i+=2){
		if(number%i==0) return 0;
	}
	return 1;

}

int main(int argc, char** argv){


	clock_t start, end; 
	clock_t start1, end1;
	double time, time1;
	unsigned long number;
	puts("Please, input your number:");
	scanf("%lu", &number);
	start = clock();
	int is = is_prime(number);
	end = clock();

	start1 = clock();
	int is1 = fast_is_prime(number);
	end1 = clock();

	time = ((double)(end-start))/CLOCKS_PER_SEC;
	time1= ((double)(end1-start1))/CLOCKS_PER_SEC;


	printf("Затраченной время в случае использования умножения в цикле = %2f \n", time);
	printf("Затраченное время в случае вынесения вычисления корня за цикл =  %2f \n", time1);

	printf("It is the %s number \n", is? "prime":"not prime");
	printf("It is the %s number \n", is1? "prime":"not prime");
	return 0;
}


